---
bookCollapseSection: false
---

# Simple Notes Application Architecture

This page contains the deployment architecture and API flowchart diagrams for Simple Notes. This application was developed in Python using the Flask Web Framework and then containerized for use with Kubernetes.

{{< hint warning >}}
**Disclaimer:** Best practices for application development are not necessarily used in the development of this application. It is meant primarily for demo purposes and to showcase how DevSecOps can be implemented with GitLab.
{{< /hint >}}

## Deployment Architecture Diagram

![](/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/images/deployment_architecture.png)

## API Flowchart Diagram

![](/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/images/api_flowchart.png)
